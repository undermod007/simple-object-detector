import React, { useEffect, useState, useRef } from "react"
import * as cocoSsd from '@tensorflow-models/coco-ssd'
import * as tf from '@tensorflow/tfjs'
import Webcam from "react-webcam"

function App() {
	
	const webCamRef = useRef()
	
	const [model, setModel] = useState()
	const [result, setResult] = useState([])
	const [isOpen, setIsOpen] = useState(false)

    const videoConstraints = {
        width: 1920,
        height: 1080
    }

	async function loadModel() {
		try {
			const dataset = await cocoSsd.load()
			setModel(dataset)
			console.log('dataset is loaded')
		} catch(err) {
			console.log(err)
			console.log('failed to load dataset')
		}
	}

	async function predict() {
		var canvas = document.getElementById('myCanvas')
		var ctx = canvas.getContext("2d")
		ctx.clearRect(0, 0, '900', '500')

		const prediction = await model.detect(document.getElementById('img'))
		setResult(prediction)
		setIsOpen(!isOpen)
		
		if(prediction.length > 0) {
			console.log(prediction)

			for(let n = 0; n < prediction.length; n++) {
				console.log(n)

				let bboxLeft = prediction[n].bbox[0]
				let bboxTop = prediction[n].bbox[1]
				let bboxWidth  = prediction[n].bbox[2]
				let bboxHeight   = prediction[n].bbox[3] - bboxTop

				console.log("bbLeft: " + bboxLeft)
				console.log("bboxTop: " + bboxTop)
				console.log("bboxWidth: " + bboxWidth)
				console.log("bboxHeight : " + bboxHeight )

				ctx.beginPath()
				ctx.font = "28px Poppins"
				ctx.fillStyle = "red"
				ctx.fillText(
					prediction[n].class + ": " + Math.round(parseFloat(prediction[n].score) * 100) + "%", bboxLeft, bboxTop
				)

				ctx.rect(bboxLeft, bboxTop, bboxWidth, bboxHeight)
				ctx.strokeStyle = "#FF0000"
				ctx.lineWidth = 3
				ctx.stroke()

				console.log('detected')
			}
		}

		setTimeout(() => {
			predict()
		}, 1000)
	}

	useEffect(() => {
		tf.ready().then(() => {
			loadModel()
		})
	}, [])

	return (
		<div className="flex flex-col justify-evenly items-center max-w-full p-10 h-auto">
			<h1 className="uppercase font-black mb-4">Simple Object Detector</h1>
			<Webcam
				audio={false}
				id="img"
				ref={webCamRef}
				screenshotQuality={1}
				screenshotFormat="image/avif"
				videoConstraints={videoConstraints}
				className="rounded-lg w-[60rem]"
			/>
			<div className="absolute z-50">
				<canvas
					id="myCanvas"
					width={'900px'}
					height={'500px'}
					className='bg-transparent hidden md:block'
				/>
			</div>
			<button
				className="bg-red-600 p-2 rounded-full mt-3 text-white w-full text-xs tracking-widest md:w-[60rem] md:text-lg md:mb-3 uppercase font-bold "
				onClick={() => predict()}
			>
				Catch
			</button>
			<h1 className="uppercase font-bold text-2xl mt-4 md:hidden">Result</h1>
			<div className={`${isOpen ? 'block' : 'hidden'} space-y-4 mb-4 md:hidden`}>
				{
					result.length > 0 ? (
						result.map((item) => (
							<div className="border-2 rounded-lg border-slate-600 p-3">
								<p className="uppercase text-sm">Class : {item.class}</p>
								<p className="uppercase text-sm">Score : {Math.round(parseFloat(item.score) * 100)}%</p>
							</div>
						))		
					) : (
						<p>{result.class}</p>
					)
				}
			</div>
			<p className="font-black">Credit : <a className="underline" href="https://portfolio-zeta-me.vercel.app/" target="_blank" rel="author">Salman Al-Majali</a></p>
        </div>
	)
}

export default App
